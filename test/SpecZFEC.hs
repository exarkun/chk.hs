module SpecZFEC (
    tests,
    prop_encode_decode_roundtrip,
) where

import Test.Tasty (
    TestTree,
    testGroup,
 )

import Control.Monad.IO.Class (
    liftIO,
 )

import Data.Either (isRight)

import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Tasty.Hedgehog

import qualified Tahoe.ZFEC as ZFEC

tests :: TestTree
tests =
    testGroup
        "ZFEC"
        [ testProperty "data round-trips through encode/decode" prop_encode_decode_roundtrip
        ]

{- | Given any `k` output blocks from @'ZFEC.encode' k n plaintext@, @'decode'
 k n@ will reproduce the @plaintext@ input.
-}
prop_encode_decode_roundtrip :: Property
prop_encode_decode_roundtrip = property $ do
    numPrimary <- forAll $ Gen.int (Range.linear 1 256)
    numSecondary <- forAll $ Gen.int (Range.linear 0 (256 - numPrimary))
    plaintext <- forAll $ Gen.bytes (Range.linear 1 65536)

    let k = numPrimary
        n = numPrimary + numSecondary

    encoded <- liftIO $ ZFEC.encodePadded k n plaintext
    annotateShow encoded

    assert $ isRight encoded
    let (Right encodedBlocks) = encoded
        tagged = zip [0 ..] encodedBlocks

    available <- forAll $ Gen.shuffle tagged

    decoded <- liftIO $ ZFEC.decodePadded k n (take k available)
    assert $ isRight decoded
    let (Right decodedPlaintext) = decoded

    diff plaintext (==) decodedPlaintext
