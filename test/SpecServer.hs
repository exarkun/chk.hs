module SpecServer (tests) where

import Control.Monad.IO.Class (MonadIO (liftIO))
import qualified Data.ByteString as BS
import Hedgehog (Property, diff, forAll, property)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Tahoe.CHK.Types (StorageServer (storageServerRead, storageServerWrite))
import Test.Tasty (TestTree, testGroup)

import Control.Monad (void, zipWithM_)
import Generators (shareNumbers, storageIndexes)
import NullServer (memoryStorageServer)
import Test.Tasty.Hedgehog (testProperty)

tests :: TestTree
tests =
    testGroup
        "Server"
        [ testProperty
            "immutable data round-trips through memoryStorageServer write / read"
            (prop_immutable_tripping memoryStorageServer)
        ]

prop_immutable_tripping :: IO StorageServer -> Property
prop_immutable_tripping newServer = property $ do
    server <- liftIO newServer
    storageIndex <- forAll storageIndexes
    shareNum <- forAll shareNumbers
    shareChunks <- forAll $ Gen.list (Range.linear 1 100) (Gen.bytes (Range.linear 1 100))

    let write' = storageServerWrite server storageIndex shareNum
        read' = storageServerRead server storageIndex shareNum
        offsets = scanl (flip $ (+) . BS.length) 0 shareChunks

    void . liftIO $ zipWithM_ (write' . fromIntegral) offsets shareChunks

    result <- liftIO read'
    diff (BS.concat shareChunks) (==) result
