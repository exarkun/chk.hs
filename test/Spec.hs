module Main where

import Data.Yaml (
    decodeEither',
 )
import Test.Tasty (TestTree, defaultMain, testGroup)

import System.IO (hSetEncoding, stderr, stdout, utf8)

import qualified Data.ByteString as B
import qualified SpecCHK
import qualified SpecCrypto
import qualified SpecMerkle
import qualified SpecServer
import qualified SpecUEB
import qualified SpecUpload
import qualified SpecUtil
import qualified SpecZFEC

tests :: [TestTree]
tests =
    [ SpecUpload.tests
    , SpecCrypto.tests
    , SpecMerkle.tests
    , SpecZFEC.tests
    , SpecUtil.tests
    , SpecUEB.tests
    , SpecServer.tests
    ]

main :: IO ()
main = do
    -- Hedgehog writes some non-ASCII and the whole test process will die if
    -- it can't be encoded.  Increase the chances that all of the output can
    -- be encoded by forcing the use of UTF-8 (overriding the LANG-based
    -- choice normally made).
    hSetEncoding stdout utf8
    hSetEncoding stderr utf8

    -- TODO Handle this reading better
    test_vectors <- B.readFile "test_vectors.yaml"
    case decodeEither' test_vectors of
        Left err -> error $ "Failed to decode test vectors: " ++ show err
        Right test_vectors' ->
            defaultMain $ testGroup "all of it" $ SpecCHK.tests test_vectors' : tests
