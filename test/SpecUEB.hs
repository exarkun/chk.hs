module SpecUEB (tests) where

import Hedgehog
import Tahoe.CHK.URIExtension
import Test.Tasty
import Test.Tasty.Hedgehog

import Text.Megaparsec

import Generators

tests :: TestTree
tests =
    testGroup
        "URIExtension"
        [ testProperty "URIExtension round-trips through put / get" prop_roundtrip
        ]

prop_roundtrip :: Property
prop_roundtrip = property $ do
    ueb <- forAll genURIExtension
    tripping ueb uriExtensionToBytes (parse pURIExtension "")
