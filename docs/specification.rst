CHK Encryption (January 22, 2021)
=================================

Here's how you encrypt and encode application data into a number of CHK shares.

Definitions
-----------

Hash(tag) means the SHA256d hash of the concatenation of tag and some string.

Numbers are represented as strings using their base 10 representation unless otherwise specified.

Base32 encoding uses the RFC4648 alphabet (Table 3), except lowercase and with no padding.

The netstring encoding of a string is the length of the string followed by ":" followed by the string followed by ",".

The leaf position in a merkle tree identifies a leaf by counting leaves from left to right starting from 0.

A merkle tree is serialized to bytes as the concatenation of the hash values at each of its nodes,
visited breadth first left to right.

Encryption
----------

CHK uses AES128 CTR-mode encryption.
This is applied to the application data to create the ciphertext.

If you don't already have a convergence secret, get 16 bytes to use as one.

The convergence encryption tag prefix is "allmydata_immutable_content_to_key_with_added_secret_v1+".

Select your Parameters (maxSegmentSize, requiredShares, totalShares).
To encode the parameters for the convergence encryption tag, join requiredShares, totalShares, and maxSegmentSize with ",".
For example, maxSegmentSize=1024, requiredShares=3, totalShares=10 encodes to "3,10,1024".

The convergence encryption tag is the concatenation of the convergence encryption tag prefix and the netstring encoding of the convergence secret and the netstring encoding of the encoded parameters.
For example, convergence secret "aaaa" and the example parameters above encode to
"allmydata_immutable_content_to_key_with_added_secret_v1+4:aaaa,9:3,10,1024,".

The 16 byte prefix of the hash(convergence encryption tag) of the application data is your AES128 encryption key.

Use this key to AES128 encrypt application data in CTR mode.
If the application data does not completely fill the final AES128 block, do not pad it.
This is your ciphertext.

Ciphertext Hash
---------------

The hash(allmydata_crypttext_v1) of the ciphertext is the Ciphertext Hash.

Main Segments
-------------

CHK divides the ciphertext into segments.
All segments except the last segment are main segments.
The real main segment size is the smallest multiple of requiredShares greater than or equal to the lesser of maxSegmentSize and the ciphertext data size.
This makes a segment evenly divisible into requiredShares pieces.
The first main segment consists of the first (real main segment size) bytes of ciphertext.
The second main segment consists of the next (real main segment size) bytes.
And so on.

Tail Segments
-------------

The last segment is the tail segment.
If the the real main segment size evenly divides the ciphertext then the tail segment is exactly the same as a main segment.

Otherwise,
the real tail segment contains whatever ciphertext is left over after all main segments have been created.

Ciphertext Hash Tree
--------------------

The ciphertext hash tree is a full merkle tree where, from left to right, the leaves are the hashes(allmydata_crypttext_segment_v1) of the segments.
If the number of segments does not exactly fill a level of the tree then the missing values are filled using hashes of a filler value.
The filler values are the hash(Merkle tree empty leaf) of the leaf position the hash fills.
The internal node hashes are given by the hash(Merkle tree internal node) of the concatenation of the netstring encoding of the left and right children.

Blocks
------

Each segment is erasure encoded using the chosen parameters (requiredShares, totalShares) to produce a list containing a number of blocks equal to totalShares.
When encoding the tail segment,
if its size is not a multiple of requiredShares then it is zero padded until it is.

Block Hash Trees
----------------

There are a number (equal to totalShares) of block hash trees.
Each block hash tree is constructed using one block from each segment from a chosen position in the erasure encoding output.
For example, the 0th block hash tree is constructed using the 0th block from erasuring encoding each segment.

A block hash tree is a full merkle tree where, from left to right, the leaves are the hashes(allmydata_encoded_subshare_v1) of the corresponding blocks.

Share Hash Tree
---------------

The share hash tree is a merkle tree where, from left to right, the leaves are the root hashes, from first to last, of the block hash trees.

Needed Share Hashes
-------------------

There are a number (equal to totalShares) of different groups of needed share hashes.
Each group of needed shares consists of the index-tagged value from a node in the share hash tree which is needed to validate the block hash tree root for a particular share.
The group is represented as the concatenation of all its elements.

A share hash tree node value is needed if it is the sibling of any node on the path from the known share hash tree leaf up to the share hash tree root.

An index-tagged value is the concatenation of a 2 byte big-endian integer and a value.
The integer gives the position in a merkle tree where the value belongs.
The position is a zero-based index into the breadth first, left-to-right traversal of the tree.

URI Extension
-------------

The URI extension is a collection of metadata about the share.
It consists of zero or more encoded key/value pairs.
Each pair is encoded as the concatenation of the key, ":", and the netstring encoding of the value.
The following keys are known:

#. codec_name: The name of the erasure encoding algorithm.
   "crs" for all known implementations.
#. codec_params: The main segment size, the required shares, and the total shares.
   The values are separated by "-".
#. tail_codec_params: The tail segment size, the required shares, and the total shares.
   The representation is the same as for codec_params.
#. size: The size of the application data
#. segment_size: The size of a main segment in this share
#. num_segments: The number of segments (main and tail) in this share
#. needed_shares: The minimum number of shares needed to reconstruct the application data
#. total_shares: The total number of shares produced by the encoding process
#. crypttext_hash: The Ciphertext Hash.
#. crypttext_root_hash: The value at the root of the Ciphertext Hash Tree.
#. share_root_hash: The value at the root of the Share Hash Tree.

The URI extension is encoded as the concatenation of all of the encoded pairs,
sorted alphabetically by key.

Shares
------

A v1 or v2 share consists of a header made of the following integers.
The version number is given by the first 4 bytes as a 4 byte big-endian integer.

For v1 all other integer values are represented as 4 byte big-endian integers.
For v2 all other integer values are represented as 8 byte big-endian integers.

#. Format version number (4 bytes)
#. The size of a single erasure encoded block
#. The size of all of the blocks for one share
#. The offset into the share of the start of the first block
#. An unused value
#. The offset into the share of the ciphertext hash tree
#. The offset into the share of the block hash tree
#. The offset into the share of the needed share hashes.
#. The offset into the share of the URI extension size

Following the header is the concatenation of all of the blocks that belong to the share.
Following the blocks are:

#. An unused space large enough for a plaintext hash tree.
#. The ciphertext hash tree
#. The block hash tree
#. The needed share hashes
#. The URI extension size
#. The URI extension

Capability
----------

The capability is the concatenation of:

#. The literal "URI:CHK:"
#. The base32 encoding of the AES128 encryption key (16 bytes, 26 base32 digits, unpadded)
#. ":"
#. The base32 encoding of the hash(allmydata_uri_extension_v1) of the encoded URI extension (32 bytes, 52 base32 digits, unpadded)
#. ":"
#. The number of required shares (positive integer)
#. ":"
#. The number of total shares (positive integer)
#. ":"
#. The size of the associated application data in bytes (positive integer)

That's all.
