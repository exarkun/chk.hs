# Changelog for tahoe-lafs-immutable-uploader

## 0.1.0

* Initial release.
* Support for encoding data using Tahoe-LAFS' CHK protocol.
