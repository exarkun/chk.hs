{
  description = "tahoe-lafs-immutable-uploader";

  inputs = {
    # Nix Inputs
    nixpkgs.url = github:nixos/nixpkgs/?ref=nixos-22.11;
    flake-utils.url = github:numtide/flake-utils;
    hs-flake-utils.url = "git+https://whetstone.private.storage/jcalderone/hs-flake-utils.git?ref=main";
    hs-flake-utils.inputs.nixpkgs.follows = "nixpkgs";

    botan-src = {
      flake = false;
      # We're using the unreleased (unmerged, even) Botan ZFEC C API.
      url = "github:PrivateStorageio/botan?ref=3190.ffi-zfec";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    hs-flake-utils,
    botan-src,
  }: let
    ulib = flake-utils.lib;

    # We currently depend on a very new (unmerged feature branch!) version of
    # Botan.  Get that version into our environment by overlaying it on top of
    # the one already in nixpkgs.
    overlay = self: super: rec {
      botan2 =
        (super.botan2.overrideAttrs (old: {
          # Use the source from the flake input.
          src = botan-src;
        }))
        .override (old: {
          # Also, turn on debugging.
          extraConfigureFlags = "--debug-mode";
        });
      # cabal2nix guesses the nixpkgs attribute wrong.  Put the thing we want
      # in the wrong place, too...
      botan = botan2;
    };

    ghcVersion = "ghc8107";
  in
    ulib.eachSystem ["x86_64-linux" "aarch64-darwin"] (system: let
      # Get a nixpkgs customized for this system and including our overlay.
      pkgs = import nixpkgs {
        inherit system;
        overlays = [overlay];
      };
      hslib = hs-flake-utils.lib {
        inherit pkgs;
        src = ./.;
        compilerVersion = ghcVersion;
        packageName = "tahoe-chk";
      };
    in {
      checks = hslib.checks {};
      devShells = hslib.devShells {
        extraBuildInputs = pkgs:
          with pkgs; [
            pkg-config
            botan2
          ];
      };
      packages = hslib.packages {};
      apps.hlint = hslib.apps.hlint {};

      # Using the working directory of `nix run`, do a build with cabal and
      # then run the test suite.
      apps.cabal-test = {
        type = "app";
        program = "${
          pkgs.writeShellApplication {
            name = "cabal-build-and-test";
            runtimeInputs = with pkgs; [pkg-config haskell.compiler.${ghcVersion} cabal-install];

            text = ''
              # It would be nice if runtimeInputs did this for us.
              export PKG_CONFIG_PATH="${pkgs.botan2}/lib/pkgconfig"

              cabal update hackage.haskell.org
              cabal build --enable-tests
              eval "$(cabal list-bin tahoe-chk-tests)"
            '';
          }
        }/bin/cabal-build-and-test";
      };
    });
}
